import React from 'react'
import styled from '@emotion/styled'
import { css } from '@emotion/react'
import { useHistory } from 'react-router'
import { useSpring, animated, config, interpolate } from 'react-spring'
import Loader from 'react-spinners/RingLoader'

import { ReactComponent as CloseIcon } from './close.svg'
import { SliceStatus, Photo as PhotoType } from '../../types'

const useModalAnimation = (show: boolean) => {
  const { opacity, scale, rotation } = useSpring({
    config: config.wobbly,
    from: { opacity: 0, scale: 0, rotation: 0.1 },
    opacity: show ? 1 : 0,
    scale: show ? 1 : 0,
    rotation: show ? 0 : 0.1,
  })

  const transform = interpolate(
    // @ts-expect-error Typings are wrong, this is the expected way
    [scale, rotation],
    (scale, rotation) => `scale(${scale}) rotate(${rotation}turn)`
  )

  return { opacity, transform }
}

const Photo: React.FC<{
  photo?: PhotoType
  status: SliceStatus
  onClose?: () => void
}> = (props) => {
  const { status, photo, onClose } = props

  const history = useHistory()

  const showModal = status === 'error' || status === 'success'
  const isLoading = status === 'loading'
  const showOverlay = status !== 'idle'

  const modalStyle = useModalAnimation(showModal)

  const renderContent = () => {
    if (photo) {
      return (
        <>
          <Image src={photo.url} />
          <Title title={photo.title}>{photo.title}</Title>
        </>
      )
    }
    if (!showOverlay || isLoading) {
      return null
    }

    if (status === 'error') {
      return 'Oops, error loading photo...'
    }

    throw new Error('<Photo>: Impossible state')
  }

  return (
    <Overlay
      show={showOverlay}
      onClick={(event) => {
        event.stopPropagation()
        onClose?.()
        setTimeout(() => {
          history.goBack()
        }, 300)
      }}
    >
      <Loader loading={status === 'loading'} css={center} color="white" />
      <Positioner>
        <animated.div style={modalStyle}>
          <CloseButton>
            <CloseIcon />
          </CloseButton>
          <Modal onClick={(event) => event.stopPropagation()}>
            {renderContent()}
          </Modal>
        </animated.div>
      </Positioner>
    </Overlay>
  )
}

const Image = styled.img`
  opacity: 100%;
  width: 100%;
  height: auto;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;

  @media (max-width: 600px) {
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
  }
`

const center = css`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`

const Overlay = styled.div<{ show: boolean }>`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 100;

  pointer-events: none;

  background: rgba(0, 0, 0, ${(props) => (props.show ? 0.5 : 0)});
  ${(props) =>
    props.show &&
    css`
      cursor: pointer;
      pointer-events: auto;
      backdrop-filter: blur(2px);
    `}

  transition: all 300ms ease-in-out;
`

const Positioner = styled.div`
  ${center};
`

const Modal = styled(animated.div)`
  background: rgba(255, 255, 255, 1);
  padding: 24px;
  border-radius: 8px;
  filter: drop-shadow(0 0 15px black);
  width: min(600px, calc(100vw - 100px));
  cursor: default;

  @media (max-width: 600px) {
    padding: 10px;
    border-radius: 6px;
    width: min(600px, calc(100vw - 60px));
  }
`

const Title = styled.h1`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 16px;
`

const CloseButton = styled.button`
  position: absolute;
  right: 0;
  bottom: calc(100% + 10px);
  background: none;
  border: none;
  outline: none;

  cursor: pointer;

  svg path {
    fill: gray;
    transition: fill 200ms ease-in-out;
  }

  :hover {
    svg path {
      fill: white;
    }
  }
`

export default Photo
