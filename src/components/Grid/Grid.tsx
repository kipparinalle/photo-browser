import React from 'react'
import styled from '@emotion/styled'
import { Link } from 'react-router-dom'
import PulseLoader from 'react-spinners/PulseLoader'

import { Photo, SliceStatus } from '../../types'
import { useAppear } from '../../hooks'

type Props = {
  basePath: string
  loadMore: () => void
  photos: Photo[]
  status: SliceStatus
  error: Error | null
}

const Grid: React.FC<Props> = (props) => {
  const { status, photos, loadMore, basePath } = props

  const scrollElement = useAppear(loadMore)

  const renderContent = () => {
    if (status === 'error') {
      return 'Oops, error loading photos...'
    }

    return photos.map((photo) => (
      <ThumbnailLink key={photo.id} to={`${basePath}/photos/${photo.id}`}>
        <Thumbnail src={photo.thumbnailUrl} />
      </ThumbnailLink>
    ))
  }

  return (
    <Wrapper>
      {renderContent()}
      <Loader ref={scrollElement}>
        <PulseLoader color={jet} loading={status === 'loading'} />
      </Loader>
    </Wrapper>
  )
}

const jet = '#333138'

const ThumbnailLink = styled(Link)`
  display: flex;
  flex: 1 0 150px;

  background: ${jet};
  border: 2px solid ${jet};
  border-radius: 5px;

  transition: all 200ms ease-in-out;

  overflow: hidden;

  :hover {
    border-color: white;
    transform: scale(1.03);
  }
`

const Wrapper = styled.div`
  margin: auto;

  display: flex;
  flex-wrap: wrap;

  width: calc(100% - 6px);

  margin: -3px;

  > * {
    margin: 3px;
  }
`

const Thumbnail = styled.img`
  width: 100%;
  height: auto;
`

const Loader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 30px;
  flex: 1 0 100%;
`

export default Grid
