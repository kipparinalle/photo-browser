import React from 'react'

import Grid from '../components/Grid'
import Photo from '../components/Photo'

import { usePhoto, usePhotoList, useTypedDispatch } from '../ducks/hooks'
import { Store } from '../ducks/store'
import { usePhotoId } from '../hooks'
import { ViewComponent } from '../types'

const Container: ViewComponent = (props) => {
  return (
    <Store>
      <GridContainer />
      <PhotoContainer />
    </Store>
  )
}

Container.path = '/redux-thunk'
Container.title = 'Redux Thunk'

export default Container

const GridContainer: React.FC = () => {
  const state = usePhotoList()

  return <Grid basePath={Container.path} {...state} />
}

const PhotoContainer: React.FC = () => {
  const photoId = usePhotoId(Container.path)
  const state = usePhoto(photoId)

  const dispatch = useTypedDispatch()
  const handleClose = () => dispatch({ type: 'photo/CLEAR' })

  return <Photo {...state} onClose={handleClose} />
}
