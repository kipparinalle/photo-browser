import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'

import Grid from '../../components/Grid'
import Photo from '../../components/Photo'

import { ViewComponent } from '../../types'
import { usePhotoId } from '../../hooks'
import { usePhoto, usePhotoList } from './hooks'

const queryClient = new QueryClient()

const Container: ViewComponent = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <GridContainer />
      <PhotoContainer />
    </QueryClientProvider>
  )
}

Container.path = '/react-query'
Container.title = 'React Query'

export default Container

const GridContainer: React.FC = () => {
  const state = usePhotoList()

  return <Grid basePath={Container.path} {...state} />
}

const PhotoContainer: React.FC = () => {
  const photoId = usePhotoId(Container.path)
  const state = usePhoto(photoId)

  return <Photo {...state} />
}
