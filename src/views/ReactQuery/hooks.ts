import { useCallback, useMemo } from 'react'
import { useQuery, useInfiniteQuery } from 'react-query'
import { throttle } from 'throttle-debounce'

import { Photo as PhotoType } from '../../types'
import PhotoService from '../../services/PhotoService'

export const usePhoto = (photoId: number | null) => {
  const enabled = Boolean(photoId)

  const { status, data: photo } = useQuery(
    ['photos', photoId],
    () => PhotoService.get(photoId ?? 0),
    { enabled }
  )

  return useMemo(() => ({ photo, status }), [photo, status])
}

export const usePhotoList = () => {
  type Result = {
    data: PhotoType[]
    nextIndex: number | null
    previousIndex: number
  }

  const query = useInfiniteQuery<Result, Error, Result>({
    queryKey: 'photos',
    queryFn: async ({ pageParam = 0 }) => {
      const LIMIT = 100

      const photos = await PhotoService.list(pageParam, LIMIT)

      const isNext = photos.length === LIMIT
      const nextIndex = isNext ? photos.length + pageParam : null

      return { data: photos, nextIndex, previousIndex: pageParam }
    },
    getPreviousPageParam: (firstPage) => firstPage.previousIndex ?? false,
    getNextPageParam: (lastPage) => lastPage.nextIndex ?? false,
  })

  const { status, data, error, fetchNextPage } = query

  const photos = useMemo(
    () => (data ? data.pages.map((page) => page.data).flat() : []),
    [data]
  )

  // Memoized because of throttling
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const loadMore = useCallback(throttle(300, fetchNextPage), [fetchNextPage])

  return useMemo(() => ({ loadMore, photos, status, error }), [
    loadMore,
    photos,
    status,
    error,
  ])
}
