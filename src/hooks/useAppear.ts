import { useRef, useState, useEffect } from 'react'
import { useIntersection, useLatest } from 'react-use'

/**
 * Tracks element view state.
 *
 * I could have utilized [react-intersection-observer's](https://github.com/thebuilder/react-intersection-observer)
 * `useInView`-hook, but since I already had `react-use` as a dependency I opted to build this.
 *
 * @returns Ref object to be attached to tracked element
 */
const useVisibility = <TElement extends HTMLElement = HTMLDivElement>() => {
  const ref = useRef<TElement>(null)
  const intersection = useIntersection(ref, { threshold: 1 })

  const [isVisible, setVisible] = useState(false)

  useEffect(() => {
    // useIntersection returns null when hook renders for some other reason
    // than visibility change so we need to track visibility state ourselves
    if (intersection) {
      setVisible(intersection.isIntersecting)
    }
  }, [intersection])

  return [ref, isVisible] as const
}

type Callback = () => void | Promise<void>

/**
 * Tracks visibility of trigger element and calls callback function
 *
 * @param callback Function to be called when element becomes visible
 * @returns Ref object to be attached to tracked element
 */
export const useAppear = <TElement extends HTMLElement = HTMLDivElement>(
  callback: Callback
) => {
  const [ref, isVisible] = useVisibility<TElement>()

  // Get stable reference to callback to avoid triggering useEffect
  const latestCallback = useLatest(callback)

  useEffect(() => {
    if (isVisible) {
      latestCallback.current()
    }
  }, [latestCallback, isVisible])

  return ref
}
