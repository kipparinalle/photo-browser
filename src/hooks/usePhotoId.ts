import { useRouteMatch } from 'react-router'

export const usePhotoId = (base: string) => {
  const match = useRouteMatch<{ id: string }>({
    path: base + '/photos/:id',
    exact: true,
  })

  if (!match) {
    return null
  }

  const photoId = parseInt(match.params.id ?? '')

  if (isNaN(photoId)) {
    return null
  }

  return photoId
}
