import { Provider } from 'react-redux'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'

import * as photo from './photo'
import * as photoCache from './photoCache'
import * as photoList from './photoList'

const rootReducer = combineReducers({
  photo: photo.reducer,
  photoList: photoList.reducer,
  photoCache: photoCache.reducer,
})

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
)

export const Store: React.FC = (props) => <Provider store={store} {...props} />

export type RootAction = photo.Action | photoList.Action
export type RootState = ReturnType<typeof rootReducer>
