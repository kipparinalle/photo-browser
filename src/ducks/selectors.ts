import { createSelector } from 'reselect'

import { RootState } from './store'
import { Photo } from '../types'

export const selectPhotoListState = (state: RootState) => state.photoList
export const selectPhotoState = (state: RootState) => state.photo
export const selectPhotoCache = (state: RootState) => state.photoCache
export const selectPhotoStatus = (state: RootState) => state.photoList.status

export const makeSelectPhotoById = () =>
  createSelector(
    selectPhotoCache,
    (state: RootState, photoId: number | null) => photoId,
    (photoCache, photoId): Photo | undefined =>
      photoId ? photoCache[photoId] : undefined
  )

export const selectPhotoList = createSelector(
  selectPhotoListState,
  selectPhotoCache,
  (photoListState, photoCache) => ({
    ...photoListState,
    photos: photoListState.photoIds
      .map((photoId) => photoCache[photoId])
      .filter(Boolean),
  })
)
