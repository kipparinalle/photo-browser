import { Photo, SliceStatus } from '../types'

export type Action =
  | { type: 'photo/FETCH' }
  | { type: 'photo/FETCH_SUCCESS'; payload: Photo }
  | { type: 'photo/FETCH_ERROR'; payload: Error }
  | { type: 'photo/CLEAR' }

type State = {
  status: SliceStatus
  error: Error | null
}

const initialState: State = {
  status: 'idle',
  error: null,
}

export const reducer = (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case 'photo/FETCH': {
      return { ...state, status: 'loading' }
    }

    case 'photo/FETCH_SUCCESS': {
      return { ...state, status: 'success' }
    }

    case 'photo/FETCH_ERROR': {
      return { ...state, status: 'error', error: action.payload }
    }

    case 'photo/CLEAR': {
      return initialState
    }

    default:
      return state
  }
}
