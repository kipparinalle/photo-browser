import { Photo, PhotoMap } from '../types'
import { RootAction } from './store'

export const reducer = (state: PhotoMap = {}, action: RootAction): PhotoMap => {
  const mapPhotos = (photos: Photo[]): PhotoMap => {
    const initial: PhotoMap = {}

    return photos.reduce(
      (photoMap, photo) => ({ ...photoMap, [photo.id]: photo }),
      initial
    )
  }

  switch (action.type) {
    case 'photoList/FETCH_SUCCESS': {
      return { ...state, ...mapPhotos(action.payload) }
    }

    case 'photo/FETCH_SUCCESS': {
      return { ...state, [action.payload.id]: action.payload }
    }

    default:
      return state
  }
}
