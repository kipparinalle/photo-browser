import { Dispatch, useCallback, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLatest } from 'react-use'
import { throttle } from 'throttle-debounce'

import { RootAction, RootState } from './store'
import * as thunks from './thunks'
import * as selectors from './selectors'

export const useTypedDispatch = () =>
  useDispatch<Dispatch<RootAction | thunks.ThunkAction>>()

export const usePhotoList = () => {
  const dispatch = useTypedDispatch()
  const photoListState = useSelector(selectors.selectPhotoList)

  const { nextIndex } = photoListState

  // Get stable reference to avoid breaking useCallback memoization
  const latestIndex = useLatest(nextIndex)

  // Memoized because of throttling
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const loadMore = useCallback(
    throttle(300, () => dispatch(thunks.getPhotos(latestIndex.current))),
    [dispatch, latestIndex]
  )

  return useMemo(() => ({ ...photoListState, loadMore }), [
    loadMore,
    photoListState,
  ])
}

export const usePhoto = (photoId: number | null) => {
  const dispatch = useTypedDispatch()

  useEffect(() => {
    if (photoId === null) {
      return
    }

    // Don't rely on photo being on cache
    dispatch(thunks.getPhoto(photoId))
  }, [dispatch, photoId])

  const getPhotoById = useMemo(() => selectors.makeSelectPhotoById(), [])

  const photo = useSelector((state: RootState) => getPhotoById(state, photoId))
  const photoState = useSelector(selectors.selectPhotoState)

  return useMemo(() => ({ ...photoState, photo }), [photo, photoState])
}
