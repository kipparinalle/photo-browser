import { ThunkAction as ReduxThunkAction } from 'redux-thunk'

import { RootState, RootAction } from './store'
import PhotoService from '../services/PhotoService'

export type ThunkAction = ReduxThunkAction<void, RootState, unknown, RootAction>

// Action creators

export const getPhotos = (start = 0): ThunkAction => async (dispatch) => {
  dispatch({ type: 'photoList/FETCH' })
  try {
    const payload = await PhotoService.list(start)

    dispatch({ type: 'photoList/FETCH_SUCCESS', payload })
  } catch (error) {
    dispatch({ type: 'photoList/FETCH_ERROR', payload: error })
  }
}

export const getPhoto = (photoId: number): ThunkAction => async (dispatch) => {
  dispatch({ type: 'photo/FETCH' })
  try {
    const payload = await PhotoService.get(photoId)

    dispatch({ type: 'photo/FETCH_SUCCESS', payload })
  } catch (error) {
    dispatch({ type: 'photo/FETCH_ERROR', payload: error })
  }
}
