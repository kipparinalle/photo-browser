import { Photo, SliceStatus } from '../types'

export type Action =
  | { type: 'photoList/FETCH' }
  | { type: 'photoList/FETCH_SUCCESS'; payload: Photo[] }
  | { type: 'photoList/FETCH_ERROR'; payload: Error }

type State = {
  status: SliceStatus
  error: Error | null
  photoIds: number[]
  nextIndex: number
}

const initialState: State = {
  status: 'idle',
  photoIds: [],
  nextIndex: 0,
  error: null,
}

export const reducer = (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case 'photoList/FETCH': {
      return { ...state, status: 'loading' }
    }

    case 'photoList/FETCH_SUCCESS': {
      const newIds = action.payload.map((photo) => photo.id)
      const allIds = state.photoIds.concat(newIds)

      // Remove possible duplicates since infinite loader sometimes dispathes
      // two requests
      const photoIds = Array.from(new Set(allIds))

      return {
        ...state,
        status: 'success',
        photoIds,
        nextIndex: photoIds.length,
      }
    }

    case 'photoList/FETCH_ERROR': {
      return { ...state, status: 'error', error: action.payload }
    }

    default:
      return state
  }
}
