export type Photo = {
  albumId: number
  id: number
  title: string
  url: string
  thumbnailUrl: string
}

export type PhotoMap = Record<number, Photo>

export type SliceStatus = 'idle' | 'error' | 'loading' | 'success'

export type ViewComponent = React.FC & { path: string; title: string }
