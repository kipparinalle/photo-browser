import { Photo } from '../types'
import { getParams, getUrl } from './api'

const list = async (start = 0, limit = 100): Promise<Photo[]> => {
  const params = getParams({
    _start: start.toString(),
    _limit: limit.toString(),
  })
  const url = getUrl('photos', params)

  const response = await fetch(url)

  if (!response.ok) {
    throw Error(response.statusText)
  }

  return response.json()
}

const get = async (photoId: number): Promise<Photo> => {
  const url = getUrl('photos', photoId.toString())

  const response = await fetch(url)

  if (!response.ok) {
    throw Error(response.statusText)
  }

  return response.json()
}

const PhotoService = { list, get }

export default PhotoService
