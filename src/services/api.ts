const API_URL = 'https://jsonplaceholder.typicode.com/'

export const getUrl = (...fragments: string[]) =>
  API_URL + fragments.filter(Boolean).join('/')

export const getParams = (params: Record<string, string>) =>
  '?' + new URLSearchParams(params).toString()
