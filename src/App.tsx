import React from 'react'
import styled from '@emotion/styled'
import { BrowserRouter, Switch, Route, NavLink } from 'react-router-dom'
import { css, Global } from '@emotion/react'

import ReduxThunk from './views/ReduxThunk'
import ReactQuery from './views/ReactQuery/ReactQuery'

const routes = [ReduxThunk, ReactQuery]

function App() {
  return (
    <BrowserRouter>
      <Global styles={globalStyles} />

      <Main>
        <LinkContainer>
          {routes.map((link) => (
            <NavLink activeClassName="is-active" to={link.path}>
              {link.title}
            </NavLink>
          ))}
        </LinkContainer>

        <Switch>
          {routes.map((route) => (
            <Route path={route.path} component={route} />
          ))}

          <Description />
        </Switch>
      </Main>
    </BrowserRouter>
  )
}

const globalStyles = css`
  html,
  body {
    background: #291f1e;

    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto',
      'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
      'Helvetica Neue', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
`

const Main = styled.main`
  margin: auto;
  width: 100vw;
  max-width: 1200px;

  display: flex;
  flex-wrap: wrap;
`

const LinkContainer = styled.nav`
  position: sticky;
  top: 10px;

  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;

  padding-bottom: 20px;

  z-index: 10;

  > * {
    margin: 0 8px;
  }

  a {
    color: white;
    background: #48284a;
    text-decoration: none;
    padding: 8px;

    border-radius: 3px;

    transition: all 300ms ease-in-out;

    :hover {
      background: #5a325d;
    }

    &.is-active {
      background: #916c80;
    }
  }
`

const Description: React.FC = (props) => {
  return (
    <DescriptionWrapper>
      <p>
        This is a page demonstrating state management handling with various
        libraries.
      </p>
      <p>Click on one of the buttons above to start</p>
    </DescriptionWrapper>
  )
}

const DescriptionWrapper = styled.div`
  flex: 1 1 auto;
  text-align: center;
  color: white;
  font-size: 24px;
`

export default App
