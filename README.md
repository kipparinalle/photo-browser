# Photo Browser

Simple photo browser that shows placeholder images.

[Demo](https://photo-browser.vercel.app/)

## Features

- Shows grid of photos from [{JSON} Placeholder](http://jsonplaceholder.typicode.com/)
- Navigating to single photo opens photo modal
- Modal animated with [react-spring](https://www.react-spring.io/)
- Written in [TypeScript](https://www.typescriptlang.org/)
- Infinite loader based on `useIntersection`-hook from [react-use](https://github.com/streamich/react-use)
- Loading states with [react-spinners](https://www.davidhu.io/react-spinners/)
- Stylings with [Emotion](https://emotion.sh/docs/introduction)

### Implemented state handling libraries

- [Redux](https://redux.js.org/) with [redux-thunk](https://github.com/reduxjs/redux-thunk)
- [react-query](https://react-query.tanstack.com/),

## Future plans

- Fix and add tests
- Navigate to next/previous photo
- Demonstrate multiple state handling libraries. For example:
  - [redux-saga](https://redux-saga.js.org/),
  - [redux-toolkit](https://redux-toolkit.js.org/),
  - [jotai](https://github.com/pmndrs/jotai),
  - [zustand](https://github.com/pmndrs/zustand),
  - [valtio](https://github.com/pmndrs/valtio)
  - Pure React with context + `useReducer`

## Quick start

Install dependencies:
`yarn`

Start project:
`yarn start`

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Boilerplate

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
